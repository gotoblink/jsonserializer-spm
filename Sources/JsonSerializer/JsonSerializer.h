//
//  JsonSerializer.h
//  JsonSerializer
//
//  Created by Warrick Walter on 3/11/16.
//  Copyright © 2016 Digitalstock. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JsonSerializer.
FOUNDATION_EXPORT double JsonSerializerVersionNumber;

//! Project version string for JsonSerializer.
FOUNDATION_EXPORT const unsigned char JsonSerializerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JsonSerializer/PublicHeader.h>


