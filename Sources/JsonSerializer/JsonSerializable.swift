import Foundation

/**
 Protocol that defines a JsonSerializeableClass
 */
public protocol JsonSerializable {
    /**
     Implement this to serialize an object to a json dictionary
     */
    func serialize() -> [String: Any]
}
