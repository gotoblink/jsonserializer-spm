import Foundation

/**
 Json serialization sanitizer, to change swift types to valid json types
 */
public class JsonSerializer {
    /**
     Serializers used to serialize arbituary types to json values
     */
    public static var valueSerializers = [JsonValueSerializer]()
    
    /**
     Sanitize a json object (dictionary or array)
     */
    public static func serializeObject(object: Any) -> Any {
        // If the object is json serializable, serialize it to json before continuing
        if let jsonSerializable = object as? JsonSerializable {
            // Serialize the result of the json serializable data
            return serializeObject(jsonSerializable.serialize())
        }
        else if let json = object as? [String: Any] {
            return serializeObject(json)
        }
        else if let json = object as? [Any] {
            return serializeArray(json)
        }
        else {
            fatalError("Invalid json object - should be a dictionary or a json")
        }
    }
    
    /**
     Sanitize a json dictionary
     */
    public static func serializeObject(_ json: [String: Any?]) -> [String: Any] {
        var dict = [String: Any]()
        
        json.forEach { (keyValue) in
            // If the value is json serializable, serialize it to an object
            if let jsonSerializable = keyValue.value as? JsonSerializable {
                dict[keyValue.key] = serializeObject(jsonSerializable.serialize())
            }
            // If the value is a dictionary, sanitize it as an object
            else if let value = keyValue.value as? [String: Any?] {
                dict[keyValue.key] = serializeObject(value)
            }
            // If the value is an array, saitize it as an array
            else if let value = keyValue.value as? [Any?] {
                dict[keyValue.key] = serializeArray(value)
            }
            else {
                // Sanitize the value, since this value isn't a dictionary
                dict[keyValue.key] = sanitizeValue(keyValue.value)
            }
        }
        
        return dict
    }
    
    /**
     Sanitize an array - this sanitizes all the objects in an array
     */
    public static func serializeArray(_ array: [Any?]) -> [Any] {
        return array.map { (object) -> Any in
            if let jsonSerializable = object as? JsonSerializable {
                return serializeObject(jsonSerializable.serialize())
            }
            else if let object = object as? [String: Any?] {
                return serializeObject(object)
            }
            else {
                return sanitizeValue(object)
            }
        }
    }
    
    /**
     Sanitize and serialize a value
     */
    public static func sanitizeValue(_ value: Any?) -> Any {
        if value == nil || value is NSNull {
            return NSNull()
        }
        else if let value = value as? String {
            return value
        }
        else if let value = value as? NSString {
            return value as String
        }
        else if let value = value as? NSNumber {
            return value
        }
        else if let value = value as? Int8 {
            return NSNumber(value: value)
        }
        else if let value = value as? Int16 {
            return NSNumber(value: value)
        }
        else if let value = value as? Int32 {
            return NSNumber(value: value)
        }
        else if let value = value as? Int64 {
            return NSNumber(value: value)
        }
        else if let value = value as? Double {
            return NSNumber(value: value)
        }
        else if let value = value as? Float {
            return NSNumber(value: value)
        }
        else {
            // Go over all the value serializers, and give them a chance to serialize the value
            for serializer in valueSerializers {
                // See if the serializer can serialize the value
                if serializer.canSerialize(type: type(of: value!)) {
                    // Sanitize the value returned by the serializer (eg, it may have returned nil, which needs converted to NSNull)
                    return sanitizeValue(serializer.serialize(value!))
                }
            }
            
            fatalError("Cannot serialize json type :\(type(of: value!))")
        }
    }
}
