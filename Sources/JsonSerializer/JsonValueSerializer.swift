import Foundation

/**
 Defines a json value serializer protocol
 */
public protocol JsonValueSerializer {
    /**
     Protocol implementations should return true from this method if they can serialize the given type
     */
    func canSerialize(type: Any.Type) -> Bool
    
    /**
      Serialize a value (which will be a valid type, where canSerialize has returned true)
     */
    func serialize(_ value: Any) -> Any?
}
