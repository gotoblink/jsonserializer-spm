import XCTest
import JsonSerializer

class ValueSerializerTests: XCTestCase {
    func testValueSerializer() {
        // Test all the different value types
        
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(nil)) is NSNull.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(NSNull())) is NSNull.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue("StringValue")) is String.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(NSString(string: "StringValue"))) is String.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(Int8(1))) is NSNumber.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(Int16(1))) is NSNumber.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(Int32(1))) is NSNumber.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(Int64(1))) is NSNumber.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(1)) is NSNumber.Type)
        
        // Add the custom value serializer
        JsonSerializer.valueSerializers.append(CustomDateSerializer())
        JsonSerializer.valueSerializers.append(CustomColorSerializer())
        
        // Test the custom value serializer
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue(Date())) is String.Type)
        XCTAssertTrue(type(of: JsonSerializer.sanitizeValue((1,1))) is NSNull.Type)
    }
}

/**
 Custom value serializer for testing
 */
class CustomDateSerializer : JsonValueSerializer {
    func canSerialize(type: Any.Type) -> Bool {
        if type == Date.self {
            return true
        }
        else {
            return false
        }
    }
    
    func serialize(_ value: Any) -> Any? {
        return "String response"
    }
}
/**
 Custom value serializer for testing
 */
class CustomColorSerializer : JsonValueSerializer {
    func canSerialize(type: Any.Type) -> Bool {
        if type == (Int, Int).self {
            return true
        }
        else {
            return false
        }
    }
    
    func serialize(_ value: Any) -> Any? {
        return nil
    }
}
