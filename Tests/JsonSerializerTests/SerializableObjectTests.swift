import XCTest
@testable import JsonSerializer

class JsonSerializerTests: XCTestCase {
    /**
     Test the custom class json serialization
     */
    func testValueSerializer() {
        let json: [String: Any] = [
            "Key": CustomJsonSerializableObject()
        ]
        
        let dict = JsonSerializer.serializeObject(json)["Key"] as! [String: Any]
        XCTAssertEqual(dict["InnerKey"] as? String, "Test_Value")
        
        XCTAssertEqual(2, (dict["InnerArray"] as! [Any]).count)
        XCTAssertEqual("Value", ((dict["InnerArray"] as! [Any]).first as! [String: Any])["Keyyyz"] as! String)
    }
}

/**
 Custom json serializable object
 */
class CustomJsonSerializableObject: JsonSerializable {
    func serialize() -> [String : Any] {
        return [
            "InnerKey": "Test_Value",
            "InnerClass": CustomInnerJsonSerializableObject(),
            "InnerArray": [CustomInnerJsonSerializableObject(), CustomInnerJsonSerializableObject()]
        ]
    }
}

class CustomInnerJsonSerializableObject: JsonSerializable {
    func serialize() -> [String : Any] {
        return [
            "Keyyyz": "Value"
        ]
    }
}
